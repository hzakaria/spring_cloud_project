package com.clientui.controller;

import com.clientui.beans.ProductBean;
import com.clientui.proxies.MicroserviceProduitProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ClientController {

    @Autowired
    MicroserviceProduitProxy mProduitProxy;

    @RequestMapping("/")
    public String accueil (Model model) {

        List<ProductBean> produits =  mProduitProxy.listeDesProduits();

        model.addAttribute("produits" , produits);

        return "Accueil";
    }
}
